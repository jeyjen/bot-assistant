package polilingv

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.HttpClient
import io.ktor.client.engine.config
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.http.*
import io.ktor.http.content.TextContent

class TinkoffProvider(endpoint: String, login: String, password: String){
    var endpoint = endpoint
    private val login = login
    private val password = password
    private val _client = HttpClient(io.ktor.client.engine.apache.Apache.config {
    }){
        install(JsonFeature){
            serializer = JacksonSerializer()
        }
    }

    private suspend fun request(verb: HttpMethod, url: String, content: String = ""): JsonNode? {
        var res = try {
            _client.request<JsonNode> {
                url("$endpoint$url")
                method = verb
                body = TextContent(content, contentType = ContentType.Application.FormUrlEncoded)
            }
        } catch (e: Exception) {null}

        return res
    }

    class Token(provider: TinkoffProvider, wuid: String, sessionId: String, operationTicket: String){
        private val provider = provider
        private val wuid = wuid
        private val sessionId = sessionId
        private val operationTicket =operationTicket

        suspend fun reconfirm(){
            provider.request(HttpMethod.Get, "/resendCode?initialOperationTicket=${operationTicket}&initialOperation=sign_up&confirmationType=SMSBYID&origin=web,ib5,platform&sessionid=${sessionId}&wuid=${wuid}")
        }

        suspend fun confirm(code: String){
            var confirmationOpt = Parameters.build {
                append("initialOperationTicket", operationTicket)
                append("initialOperation", "sign_up")
                append("confirmationData", "{\"SMSBYID\":\"${code}\"}")
            }.formUrlEncode()
            provider.request(HttpMethod.Post, "/confirm?origin=web,ib5,platform&sessionid=${sessionId}&wuid=${wuid}", confirmationOpt)
            provider.request(HttpMethod.Get, "/level_up?wuid=${wuid}&origin=web,ib5,platform&sessionid=${sessionId}&wuid=${wuid}")
        }
        suspend fun loadOperations(){
            provider.request(HttpMethod.Get, "/session_status?origin=web,ib5,platform&sessionid=${sessionId}")
            provider.request(HttpMethod.Get, "/personal_info?sessionid=${sessionId}&wuid=${wuid}")
            val operationsOpt = Parameters.build {
                append("requestsData", "[{\"key\":0,\"operation\":\"accounts_flat\",\"params\":{\"wuid\":wuid}},{\"key\":1,\"operation\":\"payments\",\"params\":{\"wuid\":wuid,\"statuses\":\"FAILED\",\"start\":1544994000000,\"end\":1545598799999}}]")
            }.formUrlEncode()
            val operationData = provider.request(HttpMethod.Post, "/grouped_requests?sessionid=${sessionId}&_methods=accounts_flat,payments", operationsOpt)
            // TODO записть операции в БД
        }
    }

    suspend fun login(): Token{
        val wuid = request(HttpMethod.Get, "/webuser")?.get("payload")?.get("wuid")?.asText()?:""
        val sessionId = request(HttpMethod.Get, "/session?origin=web,ib5,platform")?.get("payload")?.asText()?:""

        val data = Parameters.build {
            append("wuid", wuid)
            append("username", "???")
            append("password", "???")
            append("entrypoint_type", "context")
            append("fingerprint", "Mozilla/5.0 (Windows NT 10.0 Win64 x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36###1920x1200x24###-240###true###true###Chrome PDF Plugin::Portable Document Format::application/x-google-chrome-pdf~pdf;Chrome PDF Viewer::::application/pdf~pdf;Native Client::::application/x-nacl~,application/x-pnacl~")
            append("fingerprint_gpu_shading_language_version", "WebGL GLSL ES 1.0 (OpenGL ES GLSL ES 1.0 Chromium)")
            append("fingerprint_gpu_vendor", "WebKit")
            append("fingerprint_gpu_extensions_hash", "67525e96246f737a7133a8281f639650")
            append("fingerprint_gpu_extensions_count", "27")
            append("fingerprint_device_platform", "Win32")
            append("fingerprint_client_timezone", "-240")
            append("fingerprint_client_language", "ru-RU")
            append("fingerprint_canvas", "fe6505667f07db8da4e98ccca66d6adb")
            append("fingerprint_accept_language", "ru-RU,ru,en-US,en")
            append("mid", "63145977549761069803926329091002560008")
            append("device_type", "desktop&form_view_mode=desktop")
            append("utm_source", "www.google.ru")
            append("device_browser", "Chrome")
            append("device_browser_version", "")
            append("device_model", "Windows PC")
            append("device_platform", "windows")
            append("device_platform_version", "10.0")
            append("device_screen_height", "768")
            append("device_screen_size", "12.1")
            append("device_screen_width", "1024")
            append("device_vendor", "Generic")
        }.formUrlEncode()

        val operationTicketData = request(HttpMethod.Post, "/sign_up?origin=web,ib5,platform&sessionid=${sessionId}&wuid=${wuid}", data)
        val operationTicket = operationTicketData?.get("operationTicket")?.asText()?: ""

        return Token(this, wuid, sessionId, operationTicket)


    }

}