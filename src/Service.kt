package polilingv

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import org.apache.commons.lang3.RandomStringUtils
import java.lang.StringBuilder
import java.time.Instant
import java.time.ZoneOffset


fun Application.service(){
    val telegramUrl = this.environment.config.property("ktor.telegram.api").getString()
    val telegramToken = this.environment.config.property("ktor.telegram.token").getString()
    val engine = BotEngine(telegramUrl, telegramToken)

    val tinkoffUrl = this.environment.config.property("ktor.tinkoff.api").getString()
    val tinkoffLogin = this.environment.config.property("ktor.tinkoff.login").getString()
    val tinkoffPassword = this.environment.config.property("ktor.tinkoff.password").getString()
    val tinkoff = TinkoffProvider(tinkoffUrl, tinkoffLogin, tinkoffPassword)

    serviceParametrized(engine, tinkoff)
}
fun Application.serviceParametrized(engine: BotEngine, tinkoffProvider: TinkoffProvider){

    install(ContentNegotiation) {
        jackson {}
    }
    install(CORS){
        host("*")
    }
    routing {
        get("/"){
            call.respond("tinkoff bot")
        }
        post("/telegram"){
            val rawObject = call.receive<ObjectNode>()
            val chatId = rawObject.get("message").get("chat").get("id").asText()
            engine.echo(chatId,"<b>hello world</b>")
            /*
                // если сообщение на подключение
                    // отправить приветсвие и клавиатуру с меню
                // если приходит "выгрузка операций",
                    // тогда пройти авторизацию в Tinkoff
                    // отправить клавиатуру "перезапросить смс-код" "отменить"
                // если пришел четырех значный код
                    // тогда выполнить подтверждение авторизации
                    // выполнить выгрузку
                // если пришло "перезапросить смс-код"
                    // тогда перезапросить в Tinkoff смс
                    // отправить клавиатуру "перезапросить смс-код" "отменить"
                // если пришло "отменить"
                    // тогда закрыть сессию
                    // отправить клавиатуру верхнеуровнего меню
            * */
            call.respond("")
        }
    }
}