package polilingv

import com.fasterxml.jackson.databind.JsonNode
import io.ktor.client.HttpClient
import io.ktor.client.engine.config
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.http.*
import io.ktor.http.content.TextContent

class BotEngine(endpoint: String, token: String){
    private val endpoint = endpoint
    private val token = token

    private val _client = HttpClient(io.ktor.client.engine.apache.Apache.config {})
    private suspend fun send(apiMethod: String, content: String = ""): JsonNode? {
        return try {
            _client.request<JsonNode> {
                url("$endpoint/bot$token/$apiMethod")
                method = HttpMethod.Post
                body = TextContent(content, contentType = ContentType.Application.FormUrlEncoded)
            }
        } catch (e: Exception) {null}
    }

    suspend fun echo(chat_id: String, text: String){
        val message = Parameters.build {
                append("chat_id", chat_id)
                append("text", text)
                append("parse_mode", "HTML")
            }.formUrlEncode()
        send("sendMessage", message)
    }
}