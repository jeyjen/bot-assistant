package polilingv

import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import org.junit.Assert
import org.junit.Test


class BotEngineTest{
    @Test
    fun echoMessage() {
        withTestApplication( Application::service ) {
            val message = "{\"update_id\":617131670,\"message\":{\"message_id\":44,\"from\":{\"id\":344482905,\"is_bot\":false,\"first_name\":\"Eugene\",\"last_name\":\"Novikov\",\"username\":\"jayjen\",\"language_code\":\"en\"},\"chat\":{\"id\":344482905,\"first_name\":\"Eugene\",\"last_name\":\"Novikov\",\"username\":\"jayjen\",\"type\":\"private\"},\"date\":1549466535,\"text\":\"просто\"}}"
            val call = handleRequest(HttpMethod.Post, "/telegram") {
                addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                setBody(message)
            }
            with(call) {
                Assert.assertEquals(200, response.status()?.value)
            }
        }
    }
}

