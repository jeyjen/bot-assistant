package polilingv
import kotlinx.coroutines.runBlocking
import org.junit.Test


class TinkoffProviderTest{
    val wuid = "02f6a78b2af44981842cfeb2a695f47b"
    val sessionId = "E0ZMJqP22Hbr1ZMA8O3PO2YjOkwbKHDb.m1-api18"
    val operationTicket = "-5753979546501302021"
    @Test
    fun login(){
        runBlocking {
            val dd = TinkoffProvider("", "", "")
            val s = dd.login()
        }
    }
    @Test
    fun reconfirm(){
        runBlocking {
            val dd = TinkoffProvider("", "", "")
            val s = TinkoffProvider.Token(dd, wuid, sessionId, operationTicket)
            s.reconfirm()
        }
    }
    @Test
    fun loadOperation(){
        runBlocking {
            val dd = TinkoffProvider("", "", "")
            val s = TinkoffProvider.Token(dd, "6e17cf8539bd491aaab77c6bb6e188ee", "E9fCqBRiGBjJh9Awgc1KNLSuji0TofGl.ds-api12", "-8700465137999583719")
            s.confirm("3224")
        }
    }
}

